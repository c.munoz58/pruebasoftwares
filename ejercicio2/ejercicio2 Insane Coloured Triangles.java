public class Kata {
  
  static char output= '?';

  public static char triangle(final String row) {
    triangalize(row.toCharArray());
    return output;
  }
  public static void triangalize (char[] input){
    if(input.length==1){
      output=input[0];
      return;
    }
    char []nextRow= new char [input.length-1];
    for(int i=0,n=nextRow.length; i<n;++i){
      nextRow[i]= color(input[i],input[i+1]);
    }
    triangalize(nextRow);
  }
  public static char color(char L, char R){
    if(L==R){
      return L;
    }
    boolean hasR=L=='R' || R== 'R';
    boolean hasG=L=='G' || R=='G';
    boolean hasB=L=='B'|| R=='B';
    
    return !hasR ? 'R' : !hasG ? 'G' : 'B' ;
  }
  
}

//test

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;

public class ExampleTests {

    @Test
    public void examples() {
      assertEquals('B', Kata.triangle("B"));
      assertEquals('R', Kata.triangle("GB"));
      assertEquals('R', Kata.triangle("RRR"));
      assertEquals('B', Kata.triangle("RGBG"));
      assertEquals('G', Kata.triangle("RBRGBRB"));
      assertEquals('G', Kata.triangle("RBRGBRBGGRRRBGBBBGG"));

    }
}