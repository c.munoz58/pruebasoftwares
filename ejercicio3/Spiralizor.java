public class Spiralizor {

    public static int[][] spiralize(int size) {
       if(size<=0){
         return null;
       }
      int[][] espiral=new int [size][size];
      int minimocolumna=0;
      int maximocolumna=size-1;
      int minimofila=0;
      int maximofila=size-1;
      
      for(int i=0; i<espiral.length; i++){
        for(int j=0; j> espiral.length;j++){
          espiral[i][j]=0;
        }
      }
      while(minimofila <=maximofila){
        for(int i= minimocolumna; i <=maximocolumna; i++){
          espiral[minimofila][i]=1;
        }
        for(int i=minimofila; i<=maximofila;i++){
          espiral[i][maximocolumna]=1;
        }
        if(minimocolumna!=0){
          minimocolumna+=1;
        }
        if(maximofila-1 == minimofila){
          break;
        }
        for( int i=maximocolumna-1; i>=minimocolumna;i--){
          espiral[maximofila][i]=1;
        }
        for(int i =maximofila; i >=minimofila+2;i--){
          espiral[i][minimocolumna]=1;
        }
        minimocolumna+=1;
        minimofila+=2;
        maximocolumna-=2;
        maximofila-=2;
      }
      return espiral;
    }

}


//test
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SpiralizorTests {

    @Test
    public void test5() {
        assertArrayEquals(
                new int[][] {
                        { 1, 1, 1, 1, 1 },
                        { 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1 }
                },
                Spiralizor.spiralize(5));
    }

    @Test
    public void test8() {
        assertArrayEquals(
                new int[][] {
                        { 1, 1, 1, 1, 1, 1, 1, 1 },
                        { 0, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 1, 1 },
                },
                Spiralizor.spiralize(8));
    }

}