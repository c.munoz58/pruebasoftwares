public class RomanNumerals {
  private static int[] numerosActuales ={1000,900,500,400,100,90,50,40,10,9,5,4,1};
  private static String[] numerosRomanos={"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
 
  public static String toRoman(int n) {
    //inicializamos una variable
    int numeroReal = n;
    StringBuilder resultado= new StringBuilder();
    //para recorrer el arraylist de los numeros actuales
    for(int i=0;i<numerosActuales.length;i++){
      //validaremos si el numero real sera mayor o igual que el numero de numeros actuales sefun la posicion de i en el for.
      while(numeroReal>= numerosActuales[i]){
        //al numeroreal le restamos el numeros actual dentro del arraylist
        numeroReal-= numerosActuales[i];
        //al resultado le agregamos el arraylist con la posicion de i.
        resultado.append(numerosRomanos[i]);
      }
    }
    //el resultado lo parciamos a variable de tipo string.
    return resultado.toString();
  }
  public static int fromRoman(String romanNumeral) {
    String numeroRomano=romanNumeral;
      int resultado=0;
    
    for(int i=0; i<numerosRomanos.length;i++){
      while(numeroRomano.startsWith(numerosRomanos[i])){
        numeroRomano = numeroRomano.substring(numerosRomanos[i].length(), numeroRomano.length());
        resultado+= numerosActuales[i];
      }
    }
    return resultado;
  }
}

//test
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RomanNumeralsTest {

    @Test
    public void testToRoman() throws Exception {
        assertThat("1 converts to 'I'", RomanNumerals.toRoman(1), is("I"));
        assertThat("2 converts to 'II'", RomanNumerals.toRoman(2), is("II"));
    }

    @Test
    public void testFromRoman() throws Exception {
        assertThat("'I' converts to 1", RomanNumerals.fromRoman("I"), is(1));
        assertThat("'II' converts to 2", RomanNumerals.fromRoman("II"), is(2));
    }
}