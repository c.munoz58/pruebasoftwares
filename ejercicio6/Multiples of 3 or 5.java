public class Solution {

  public int solution(int number) {
    //se crea variable inicializada en 0
    int sumanumeros = 0;
    //se recore el largo del número ingresado
    for(int i=0;i<number;i++){
      //se filtra, si el resto dividido en 5 o 0 y el resto divido en 3 es 0. 
      if(i % 5==0 || i % 3==0){
        //se sumaria a la variable anteriormente inicializada, que seria a la suma de los números con resto 0.
        sumanumeros+=i;
      }else{
      }
    }
    return sumanumeros;
  }
}

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {
    @Test
    public void test() {
      assertEquals(23, new Solution().solution(10));
    }
    
}