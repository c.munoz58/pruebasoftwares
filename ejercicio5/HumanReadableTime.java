public class HumanReadableTime {
  public static String makeReadable(int seconds) {
    // Entregamos primero el formato de las horas, dado el segun se divide primero por 3600 para dar las horas, lo mismo seria para los minutos solo que se saca el resto dividido por 60 y por ultimo los segundos que se saca el resto dividido en 60
    return String.format("%02d:%02d:%02d",seconds / 3600,(seconds % 3600)/60,(seconds%60));
  }
}

//test
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ExampleTest {
  @Test
  public void Tests() {
    assertEquals("makeReadable(0)", "00:00:00", HumanReadableTime.makeReadable(0));
    assertEquals("makeReadable(5)", "00:00:05", HumanReadableTime.makeReadable(5));
    assertEquals("makeReadable(60)", "00:01:00", HumanReadableTime.makeReadable(60));
    assertEquals("makeReadable(86399)", "23:59:59", HumanReadableTime.makeReadable(86399));
    assertEquals("makeReadable(359999)", "99:59:59", HumanReadableTime.makeReadable(359999));
  }
}
