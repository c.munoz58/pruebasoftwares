public class Plural{

  public static boolean isPlural(float f){
    // si el número ingresado es distinto a uno, entraria en el if y retornaria un true.
    if(f!=1){
     return true; 
    }
    //en caso contrario retornaria un false.
     return false;
  }
}

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;


public class PluralTest {
   @Test
    public void BasicTest() {
      assertEquals(true,Plural.plural(0),"plural para 0");
      assertEquals(true,Plural.pural(0.5),"plural para 0.5");
      assertEquals(false,Plural.pural(1),"plural para 1");
      assertEquals(true,Plural.pural(100),"plural para 100");
      
    }
}